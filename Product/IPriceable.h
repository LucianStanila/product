#pragma once
struct IPriceable
{
	virtual uint8_t GetVAT() const = 0;
	virtual float GetPrice() const  = 0;

};

