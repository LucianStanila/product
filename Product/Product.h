#pragma once
#include<iostream>
#include <cstdint>
#include<string>
#include "IPriceable.h"
class Product: public IPriceable
{

public:
	Product(uint16_t id, const std::string& name, float price, uint16_t tva, const std::string& dateOrType);
	friend 	std::ostream& operator<<(std::ostream& os, const Product& dt);
	uint16_t getId() {
		return m_id;
	}
	std::string getName() {
		return m_name;
	}
    float FinalPrice();

protected:

	uint16_t m_id;
	std::string m_name;
	float m_rawPrice;

};

