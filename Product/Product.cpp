#include "Product.h"

Product::Product(uint16_t id, const std::string& name, float price, uint16_t tva, const std::string& dateOrType) :
	m_id(id), m_name(name), m_rawPrice(price), m_tva(tva), m_dateOrType(dateOrType) {

}

float Product::FinalPrice()
{
	return m_rawPrice+m_rawPrice*m_tva/100;
}

std::ostream& operator<<(std::ostream& os, const Product& dt)
{
	return os << dt.m_id << " " << dt.m_name << " " << dt.m_rawPrice << " " << dt.m_tva << " " << dt.m_dateOrType;
}

