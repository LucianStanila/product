#include <fstream>
#include <vector>
#include "Product.h"
#include<algorithm>
bool comparatorName(Product prod1, Product prod2)
{
	return prod1.getName() < prod2.getName();
}
bool comparatorPrice(Product prod1, Product prod2)
{
	return prod1.getPrice() < prod2.getPrice();
}
void sortByName(std::vector<Product>& vectorProd)
{

	for (int i = 0; i < vectorProd.size(); i++)
	{
		std::sort(vectorProd.begin(), vectorProd.end(), comparatorName);
	}
}
void sortByPrice(std::vector<Product>& vectorProd)
{
	for (int i = 0; i < vectorProd.size(); i++)
	{
		std::sort(vectorProd.begin(), vectorProd.end(), comparatorPrice);
	}
}
int main() {

	std::ifstream productsFile("Product.prodb");
	std::vector <Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t tva;
	std::string dateOrType;

	while (!productsFile.eof()) {

		productsFile >> id >> name >> price >> tva >> dateOrType;

		products.push_back(Product(id, name, price, tva, dateOrType));

	}
	std::cout << " NonperishableProducts on the screen........................................"<<std::endl;
	for (int i = 0; i < products.size(); i++)
	{
		if (products[i].getTva() == 19)
		{
			std::cout << products[i].getName() << " " << products[i].FinalPrice();
			std::cout<<std::endl;
		}
	}
	std::cout << " Sort by Name......................" << std::endl;
	sortByName(products);
	for (int i = 0; i < products.size(); i++)
	{
		std::cout << products[i]<<std::endl;
	}
	sortByPrice(products);
	std::cout << " Sort by Price......................" << std::endl;
	for (int i = 0; i < products.size(); i++)
	{
		std::cout << products[i] << std::endl;
	}
}